import express, { Request, Response, Express, NextFunction } from "express";
import axios from "axios";
import path from "path";

const app: Express = express();
const port = process.env.PORT || 4000;

app.use(express.json());

app.use(express.static(path.join(process.cwd(), "/../app/dist")));
app.get("/", (_req: Request, _res: Response, _next: NextFunction) => {
  _res.sendFile(path.join(process.cwd(), "/../app/dist", "index.html"));
});

app.get(
  "/representatives/:state",
  (req: Request, res: Response, _next: NextFunction) => {
    axios
      .get(
        `http://whoismyrepresentative.com/getall_reps_bystate.php?state=${req.params.state}&output=json`
      )
      .then((response: any) => {
        res.status(200).send(response.data.results);
      })
      .catch((err: any) => {
        res.status(err.status).send(err.message);
      });
  }
);

app.get(
  "/senators/:state",
  (req: Request, res: Response, _next: NextFunction) => {
    axios
      .get(
        `http://whoismyrepresentative.com/getall_sens_bystate.php?state=${req.params.state}&output=json`
      )
      .then((response: any) => {
        res.status(200).send(response.data.results);
      })
      .catch((err: any) => {
        res.status(err.status).send(err.message);
      });
  }
);

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
  console.log(
    "\x1b[32m",
    `If in dev mode please view the frontend at http://localhost:3000`
  );
});
