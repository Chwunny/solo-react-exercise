import React from "react";
import "./Header.scss";

const Header = () => {
  return (
    <>
      <h1 id="main-header">Who's My Representative?</h1>
      <div className="partition-line"></div>
    </>
  );
};

export default Header;
