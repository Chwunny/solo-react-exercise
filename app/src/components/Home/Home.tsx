import { useState } from "react";
import "./Home.scss";

import Header from "../Header";
import Dropdown from "./Dropdown";
import axios from "axios";

import statesData from "./states-data";

interface IResult {
  district: string;
  link: string;
  name: string;
  office: string;
  party: string;
  phone: string;
  state: string;
}

interface ISelectedResult {
  firstName: string;
  lastName: string;
  district: string;
  phone: string;
  office: string;
}

const options = ["Representatives", "Senators"];
const states = statesData;

const BASE_SERVER_URL = import.meta.env.DEV ? "/api" : "";

const Home = () => {
  const [repSelection, setRepSelection] = useState("");
  const [stateSelection, setStateSelection] = useState("");

  const [responseData, setResponseData] = useState<IResult[]>([]);
  const [selectedResult, setSelectedResult] = useState<ISelectedResult>();

  const submitHandler = async (): Promise<void> => {
    // Check to make sure both values are selected
    if (!repSelection || !stateSelection) {
      alert("Please select valid parameters");
      return;
    }

    await axios
      .get(`${BASE_SERVER_URL}/${repSelection}/${stateSelection}`) // ex: http://localhost:4000/Representatives/UT || http://localhost:4000/Senators/CA
      .then((res) => setResponseData(res.data));
  };

  const selectResultHandler = (result: IResult): void => {
    const newResult: ISelectedResult = {} as ISelectedResult;

    // Use spread operator below to avoid mutating any of the original data
    newResult.firstName = { ...result }.name.split(" ")[0];
    newResult.lastName = { ...result }.name.split(" ")[1];
    newResult.district = result.district;
    newResult.phone = result.phone;
    newResult.office = result.office;

    setSelectedResult(newResult);
  };
  return (
    <main className="home-component">
      <Header />
      <section className="content">
        <div className="button-container">
          <Dropdown
            values={options}
            title={"Rep / Sen"}
            stateHandler={setRepSelection}
          ></Dropdown>
          <Dropdown
            values={states}
            title={"State"}
            stateHandler={setStateSelection}
          ></Dropdown>
          <button className="submit-button" onClick={submitHandler}>
            Submit
          </button>
        </div>

        <div className="results-container">
          <div className="results-list">
            <h2 className="black">List / </h2>
            <h2 className="blue">Representatives</h2>
            <div className="results-data">
              <div className="title-container">
                <h2>Name</h2>
                <h2>Party</h2>
              </div>

              <div className="scrollable">
                {(responseData.length &&
                  responseData.map((result, idx) => {
                    return (
                      <div
                        className="result-container"
                        key={idx}
                        onClick={() => selectResultHandler(result)}
                      >
                        <h2>{result.name}</h2>
                        <h2>{result.party.slice(0, 1)}</h2>
                      </div>
                    );
                  })) ||
                  null}
                {/* The above || null check is to stop any empty artifacts from being rendered if there is no valid data to be consumed */}
              </div>
            </div>
          </div>

          <div className="selected-info">
            <h2>Info</h2>
            <div className="fields-container">
              <div className="info-field">
                <h2>{selectedResult?.firstName || "First Name"}</h2>
              </div>
              <div className="info-field">
                <h2>{selectedResult?.lastName || "Last Name"}</h2>
              </div>
              <div className="info-field">
                <h2>{selectedResult?.district || "District"}</h2>
              </div>
              <div className="info-field">
                <h2>{selectedResult?.phone || "Phone"}</h2>
              </div>
              <div className="info-field">
                <h2>{selectedResult?.office || "Office"}</h2>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
};

export default Home;
