import React, { SetStateAction, useState, Dispatch } from "react";
import "./Dropdown.scss";

interface IDropdownProps {
  values: string[];
  title: string;
  stateHandler: Dispatch<SetStateAction<string>>;
}

const Dropdown: React.FC<IDropdownProps> = ({
  values,
  title,
  stateHandler,
}) => {
  const [show, setShow] = useState(false);
  const [selected, setSelected] = useState<string>("");

  const selectionHandler = (value: string) => {
    stateHandler(value);
    setSelected(value);
    setShow(false);
  };

  return (
    <div className="dropdown-parent">
      <div className="dropdown-button no-select" onClick={() => setShow(!show)}>
        {selected || title}
      </div>
      {show && (
        <div className="dropdown-show">
          {values.map((value, idx) => {
            return (
              <div
                className="dropdown-item"
                key={idx}
                onClick={() => selectionHandler(value)}
              >
                {value}
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default Dropdown;
