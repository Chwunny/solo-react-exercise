import { useEffect } from "react";
import axios from "axios";

import Home from "./components/Home";

function App() {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;
